var React = require("-aek/react");
var Container = require("-components/container");
var {VBox} = require("-components/layout");
var {BannerHeader} = require("-components/header");
var {AekReactRouter,RouterView} = require("-components/router");

var IndexPage = require("./pages/index");
var NewsPage = require("./pages/news");

var router = new AekReactRouter();

var Screen = React.createClass({

  selectSport:function(news){
    router.goto("/feed/" + news);
  },

  render:function() {

    return (
      <Container>
        <VBox>
          <BannerHeader theme="alt" key="header" flex={0}>UAB News</BannerHeader>
          <RouterView router={router}>
            <IndexPage path="/" onSelect={this.selectSport} />
            <NewsPage path="/feed/:news" />
          </RouterView>
        </VBox>
      </Container>
    );

  }

});

React.render(<Screen/>,document.body);
