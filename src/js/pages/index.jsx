var React = require("-aek/react");
var Page = require("-components/page");
var {BasicSegment} = require("-components/segment");
var {Listview} = require("-components/listview");

var IndexPage = React.createClass({
  onClick: function(item,ev){
    ev.preventDefault();
    var code = item.Code;
    this.props.onSelect(code);
  },

  render:function(){
    var news = [
                    {text:"Latest Updates",Code:"latest"},
                    {text:"Student Experience",Code:"student-experience"},
                    {text:"Service to Community",Code:"service"},
                    {text:"Focus on Patient Care",Code:"focus-on-patient-care"},
                    {text:"Faculty Excellence",Code:"faculty"},
                    {text:"Experiencing the Arts",Code:"experiencing-the-arts"},
                    {text:"Enhancing Facilities",Code:"facilities"},
                    {text:"Innovation & Development",Code:"innovation"}
                 ];
    return (
      <Page>
        <BasicSegment>
          <Listview items={news} uniformLabels basicLabel onClick={this.onClick} />
        </BasicSegment>
      </Page>
    );
  }
});

module.exports = IndexPage;
